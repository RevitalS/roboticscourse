import csv

#Clculate the Ei average.
def Calculate_histogram(inputFile, colName, writer):
    histoDict1 = {0.0: 0,
                  0.1: 0,
                  0.2: 0,
                  0.3: 0,
                  0.4: 0,
                  0.5: 0,
                  0.6: 0,
                  0.7: 0,
                  0.8: 0,
                  0.9: 0,
                  1.0: 0
                  }
    histoDict2 = {0.0: 0,
                  0.1: 0,
                  0.2: 0,
                  0.3: 0,
                  0.4: 0,
                  0.5: 0,
                  0.6: 0,
                  0.7: 0,
                  0.8: 0,
                  0.9: 0,
                  1.0: 0
                  }
    #claculate histogram.
    for line in inputFile:
        a = line.split(",")
        histoDict1[float("{0:.1f}".format(float(a[5])))] += 1
        histoDict2[float("{0:.1f}".format(float(a[6])))] += 1
    print histoDict1
    print histoDict2
    histoDict1[0.9] += histoDict1[1.0]
    histoDict2[0.9] += histoDict2[1.0]
    colName1 = colName + "1"
    writer.writerow({'/': colName1, '0.0': histoDict1[0.0], '0.1': histoDict1[0.1], '0.2': histoDict1[0.2], '0.3': histoDict1[0.3], '0.4': histoDict1[0.4], '0.5': histoDict1[0.5], '0.6': histoDict1[0.6], '0.7': histoDict1[0.7], '0.8':histoDict1[0.8], '0.9': histoDict1[0.9]})
    colName2 = colName + "2"
    writer.writerow({'/': colName2,'0.0': histoDict2[0.0], '0.1': histoDict2[0.1], '0.2': histoDict2[0.2], '0.3': histoDict2[0.3], '0.4': histoDict2[0.4], '0.5': histoDict2[0.5], '0.6': histoDict2[0.6], '0.7': histoDict2[0.7], '0.8':histoDict2[0.8], '0.9': histoDict2[0.9]})

#    colList[0.0] += str(histoDict1) + "--" + str(histoDict2) + ","
  #  return colList

def Calculate_histogram_nop(inputFile, colName, writer):
    histoDict1 = {0.0: 0,
              0.1: 0,
              0.2: 0,
              0.3: 0,
              0.4: 0,
              0.5: 0,
              0.6: 0,
              0.7: 0,
              0.8: 0,
              0.9: 0,
              1.0: 0
              }
    #claculate histogram.
    for line in inputFile:
        a = line.split(",")
        histoDict1[float("{0:.1f}".format(float(a[5])))] += 1
    print histoDict1
    histoDict1[0.9] += histoDict1[1.0]
    colName1 = colName + "1"
    writer.writerow({'/': colName1, '0.0': histoDict1[0.0], '0.1': histoDict1[0.1], '0.2': histoDict1[0.2], '0.3': histoDict1[0.3], '0.4': histoDict1[0.4], '0.5': histoDict1[0.5], '0.6': histoDict1[0.6], '0.7': histoDict1[0.7], '0.8':histoDict1[0.8], '0.9': histoDict1[0.9]})



#open new file for the statistics.
csvfile = open('HistogramFile.csv', 'w')
fieldnames = ['/', '0.0', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9']
#fieldnames = ['/', 'Rapel*Repel', 'Repel*Noise', 'Repel*Aggression', 'Repel*Original', 'Repel*Bestevade', 'Repel*Nop',
#              'Noise*Noise', 'Noise*Aggression', 'Noise*Original', 'Noise*Bestevade', 'Noise*Nop',
#              'Aggression*Aggression', 'Aggression*Original', 'Aggression*Bestevade', 'Aggression*Nop',
#              'Original*Original', 'Original*Bestevade', 'Original*Nop',
#              'Bestevade*Bestevade', 'Repel*Nop', 'Nop*Nop']
writer = csv.DictWriter(csvfile, fieldnames=fieldnames, lineterminator='\n')
writer.writeheader()

dict = {}
switcher = {
    0: "Repel",
    1: "Noise",
    2: "Aggression",
    3: "Original",
    4: "Bestevade",
    5: "Nop"
}
colList = {0.0: "",
      0.1: "",
      0.2: "",
      0.3: "",
      0.4: "",
      0.5: "",
      0.6: "",
      0.7: "",
      0.8: "",
      0.9: "",
      1.0: ""
      }
for i in range (6):
    for j in range (i, 6):
        str1 = str(i) + str(j)
        inputFile = open( str1 + '.csv', 'r')
        #Skip the header line.
        headline = inputFile.readline()
        colName = switcher[i] + "*" + switcher[j]
        print colName


        if (j != 5):
            Calculate_histogram(inputFile, colName, writer)
        else:
            Calculate_histogram_nop(inputFile, colName, writer)