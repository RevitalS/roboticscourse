import csv

#Clculate the Ei average.
def Calculate_Ei_from_file(inputFile):
    average1 = 0
    average2 = 0
    count = 0
    str1 = ''
    #claculate Ei of one line
    for line in inputFile:
        a = line.split(",")
        #i_a1 2/i_a1+ i_p1
      #  if (a[1] != 0 or a[2] != 0):
        Ei1 = float(a[1])/(float(a[1]) + float(a[2]))
       # if (a[3] != 0 or a[4] != 0):
        Ei2 = float(a[3])/(float(a[3]) + float(a[4]))
        average1 += Ei1
        average2 += Ei2
        count += 1
    if (count != 0):
        average1 = float(average1)/count
        average2 = float(average2)/count
    average1 = float("{0:.3f}".format(average1))
    average2 = float("{0:.3f}".format(average2))
    str1 = str(average1) + '--' + str(average2)
    return str1

#Clculate the Ei average.
def Calculate_Ei_from_file_for_nop(inputFile):
    average1 = 0
    count = 0
    str1 = ''
    #claculate Ei of one line
    for line in inputFile:
        a = line.split(",")
        #i_a1 2/i_a1+ i_p1
        if (a[1] != 0 or a[2] != 0):
            Ei1 = float(a[1])/(float(a[1]) + float(a[2]))
        average1 += Ei1
        count += 1
    if (count != 0):
        average1 = float(average1)/count
    average1 = float("{0:.3f}".format(average1))
    str1 = str(average1)
    return str1

#open new file for the statistics.
csvfile = open('FinalMatrix.csv', 'w')
fieldnames = ['/','Rapel', 'Noise', 'Aggression', 'Original', 'Bestevade', 'Nop']
writer = csv.DictWriter(csvfile, fieldnames=fieldnames, lineterminator='\n')
writer.writeheader()

dict = {}
for i in range (6):
    for j in range (i, 6):
        str1 = str(i) + str(j)
        inputFile = open( str1 + '.csv', 'r')
        #Skip the header line.
        headline = inputFile.readline()
        if (j != 5):
            dict[str1] = Calculate_Ei_from_file(inputFile)
        else:
            dict[str1] = Calculate_Ei_from_file_for_nop(inputFile)

writer.writerow({'/': 'Rapel', 'Rapel': dict['00'], 'Noise': dict['01'], 'Aggression': dict['02'], 'Original': dict['03'], 'Bestevade': dict['04'], 'Nop': dict['05']})
writer.writerow({'/': 'Noise', 'Rapel': '--', 'Noise': dict['11'], 'Aggression': dict['12'], 'Original': dict['13'], 'Bestevade': dict['14'], 'Nop': dict['15']})
writer.writerow({'/': 'Aggression', 'Rapel': '--', 'Noise': '--', 'Aggression': dict['22'], 'Original': dict['23'], 'Bestevade': dict['24'], 'Nop': dict['25']})
writer.writerow({'/': 'Original', 'Rapel': '--', 'Noise': '--', 'Aggression': '--', 'Original': dict['33'], 'Bestevade': dict['34'], 'Nop': dict['35']})
writer.writerow({'/': 'Bestevade', 'Rapel': '--', 'Noise': '--', 'Aggression': '--', 'Original': '--', 'Bestevade': dict['44'], 'Nop': dict['45']})