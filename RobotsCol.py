import csv

writeDict = {}

def open_all_new_files():
    fdList = [];
    for i in range (6):
        for j in range (i, 6):
            str1 = str(i) + str(j)
            csvfile = open( str1 + '.csv', 'w')
            fieldnames = ['time', 'i_a1', 'i_p1', 'i_a2', 'i_p2', 'Ei1', 'Ei2']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, lineterminator='\n')
            writer.writeheader()
            fdList.append(csvfile)
            writeDict[str1] = writer

    #print fdList
    return fdList

def put_in_txt(r1, r2, writer):
    writer.writerow({'time': r1[0], 'i_a1': r1[2], 'i_p1': r1[3], 'i_a2': r2[2], 'i_p2': r2[3], 'Ei1': r1[4], 'Ei2':r2[4]})

def put_in_txt_nop(r1, writer):
    writer.writerow({'time': r1[0], 'i_a1': r1[2], 'i_p1': r1[3], 'Ei1': r1[4]})

def find_writer(x,y):
    if (x <= y):
        return (str(x) + str(y), False)
    else:
        return (str(y) + str(x), True)

inputFile = open('SimulationLog1.csv', 'r')

writersList = open_all_new_files()
arr = []
flag = False
dict = {}
switcher = {
    "Repel": 0,
    "Noise": 1,
    "Aggression": 2,
    "Original": 3,
    "Bestevade": 4
}
#Skip the header line.
headline = inputFile.readline()

#Insert all the relevant info to array f array
for line in inputFile:
    a = line.split(",")
    b = []
    if len(a) >= 18:
        #time 0
        b.append(a[0])
        #robot 1
        b.append(a[1])
        #i_a 2
        b.append(a[2])
        #i_p 3
        b.append(a[3])
        #Ei
        b.append(a[5])
        #method name 4
        #print a[7]
        b.append(switcher[a[7]])
        #conf.id 5..
        #b.append(a[17])
        for i in range(17, len(a), 4):
            if (a[i].isdigit()):
                 b.append(a[i])
        #add to the work arr
        arr.append(b)

#arr.remove(arr[0])
print len(arr)

count = 0
e = []
dictCon = {}
pers = []
size = len(arr)
#counter = 0
#For each robot in the array check if there is conflict
for i in range(size):
    c = arr[i]
    time = c[0]
    r = c[1]
    counter = 0
    #con = c[4]
   # for x in range(4, len(c)):
    #    con = c[x]

    #arrC = arr
    if (len(arr) == 0):
        break
    for j in range(i + 1, len(arr)):

        d = arr[j]
        timeC = d[0]
        rC = d[1]
        conC = d[5]

        #if we found all the conflicts.
        lenDictCon = 0
        if (j == len(arr) - 1):
            lenDictCon = 0
            if (i in dictCon):
           # if (dictCon.has_key(j)):
                lenDictCon = len(dictCon[i])
            # put the nops.
        if (lenDictCon + counter == (len(c) - 6)):
            break
        #check if has conflict before
        #if (i in dictCon):
         #   if(rC in dictCon[i]):
          #      break


        if (int(timeC) <= (int(time) + 500000000)):
            for x in range(6, len(c)):
                if (counter == len(c) - 6):
                    break
                flag = False
                if (c[x] == rC):
                    for y in range(6, len(d)):
                        if (r == d[y]):
                            counter += 1
                            flag = True
                            tup = find_writer(c[5], d[5])
                            writer = writeDict[tup[0]]
                            if (tup[1]):
                                put_in_txt(d, c, writer)
                            else:
                                put_in_txt(c, d, writer)
                            if (j in dictCon):
                           # if (dictCon.has_key(j)):
                                w = dictCon[j]
                                if (c[1] in w):
                                    break
                                w.append(c[1])
                            else:
                                w = []
                                w.append(c[1])
                                dictCon[j] = w

                            e.append(c)
                            break
            #if we found all the conflicts.
            lenDictCon = 0
            if (j == len(arr) - 1):
                lenDictCon = 0
                if (i in dictCon):
               # if (dictCon.has_key(j)):
                    lenDictCon = len(dictCon[i])
                # put the nops.
            if (lenDictCon + counter == (len(c) - 6)):
                break
            if (flag):
                break
            if (j == len(arr) - 1):
                lenDictCon = 0
                if (i in dictCon):
               # if (dictCon.has_key(j)):
                    lenDictCon = len(dictCon[i])
                # put the nops.
                if (lenDictCon + counter < (len(c) - 6)):
                    for q in range(len(c) - 6 - (lenDictCon + counter)):
                        writer = writeDict[find_writer(c[5], 5)[0]]
                        put_in_txt_nop(c, writer)
                        count += 1

        else:
            #put the nops
            lenDictCon = 0
            if (i in dictCon):
           # if (dictCon.has_key(j)):
                lenDictCon = len(dictCon[i])
            if (lenDictCon + counter < (len(c) - 6)):
                for q in range(len(c) - 6 - (lenDictCon + counter)):
                    writer = writeDict[find_writer(c[5], 5)[0]]
                    put_in_txt_nop(c, writer)
                    count += 1
            #if x == len(c):
 #           counter = 0
            break
print len(e)
print count